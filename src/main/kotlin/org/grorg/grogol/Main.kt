package org.grorg.grogol

typealias World = List<List<Cell>>

enum class Cell(private val representation: Char) {
    ALIVE('#'), DEAD('-');

    override fun toString() = representation.toString()
}

fun worldRepresentation(world: World): String =
        world.fold("") { acc, arrayOfCells ->
            val line = arrayOfCells.fold("") { lineRepresentation, cell ->
                "$lineRepresentation$cell"
            }
            "$acc$line\n"
        }

fun listOfNeighbors(world: World, x: Int, y: Int): List<Cell> {
    val length = world[0].size
    val height = world.size

    return listOf(
            Pair(x -1, y -1), Pair(x, y -1), Pair(x +1, y -1),
            Pair(x -1, y),  /*Pair(x, y), */ Pair(x +1, y),
            Pair(x -1, y +1), Pair(x, y +1), Pair(x +1, y +1))
            .filter { it.first in 0..(length -1) && it.second in 0..(height -1) }
            .map { world[it.second][it.first] }
}

fun numberOfAliveNeighbors(neighbors: List<Cell>): Int =
        neighbors.fold(0) { acc, cell ->
            acc + when (cell) {
                Cell.ALIVE -> 1
                Cell.DEAD -> 0
            }
        }


fun newGeneration(world: World): World =
    world.mapIndexed { y, lines ->
        lines.mapIndexed { x, cell ->
            val numOfAliveNeighbors = numberOfAliveNeighbors(listOfNeighbors(world, x, y))
            when (cell) {
                Cell.DEAD -> if (numOfAliveNeighbors == 3) {
                    Cell.ALIVE
                } else {
                    Cell.DEAD
                }
                Cell.ALIVE -> if (numOfAliveNeighbors in 2..3) {
                    Cell.ALIVE
                } else {
                    Cell.DEAD
                }
            }
        }
    }

fun main(args: Array<String>) {
    /* val world = listOf(
            listOf(Cell.DEAD, Cell.DEAD, Cell.DEAD),
            listOf(Cell.ALIVE, Cell.ALIVE, Cell.ALIVE),
            listOf(Cell.DEAD, Cell.DEAD, Cell.DEAD)) */

    val world = listOf(
            listOf(Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD),
            listOf(Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD),
            listOf(Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD),
            listOf(Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.ALIVE, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.ALIVE, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD),
            listOf(Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.ALIVE, Cell.ALIVE, Cell.DEAD, Cell.ALIVE, Cell.ALIVE, Cell.ALIVE, Cell.ALIVE, Cell.DEAD, Cell.ALIVE, Cell.ALIVE, Cell.DEAD, Cell.DEAD, Cell.DEAD),
            listOf(Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.ALIVE, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.ALIVE, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD),
            listOf(Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD),
            listOf(Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD),
            listOf(Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD, Cell.DEAD))


    generateSequence(world) { newGeneration(it) }
            .forEach {
                println(worldRepresentation(it))
                Thread.sleep(1000)
            }
}
